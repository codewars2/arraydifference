﻿using System;

namespace ArrayDifference
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                string[] strA = args[0].Split(',');
                string[] strB = args[1].Split(',');

                int[] a = new int[strA.Length];
                int[] b = new int[strB.Length];

                for (int i = 0; i < strA.Length; i++)
                {
                    a[i] = int.Parse(strA[i]);
                }

                for (int i = 0; i < strB.Length; i++)
                {
                    b[i] = int.Parse(strB[i]);
                }

                var diff = Algorithms.ArrayAlgorithmService.ArrayDiff(a, b);
                foreach (var item in diff)
                {
                    Console.Write($"{item}\t");
                }
            }
            else
            {
                int[] a = new int[] { 2, 3, 1, 1, 12, 90, -5 };
                int[] b = new int[] { 1, 2, 3, 12 };

                var diff = Algorithms.ArrayAlgorithmService.ArrayDiff(a, b);
                foreach (var item in diff)
                {
                    Console.Write($"{item}\t");
                }
            }
        }
    }
}
