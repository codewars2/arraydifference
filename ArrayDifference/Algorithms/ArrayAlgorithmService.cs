﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayDifference.Algorithms
{
    class ArrayAlgorithmService
    {
        public static int[] ArrayDiff(int[] a, int[] b)
        {
            List<int> result = new List<int>();
            bool isCrossing = false;

            foreach (var x in a)
            {
                foreach (var y in b)
                {
                    if (x == y) isCrossing = true;
                }

                if (!isCrossing)
                {
                    result.Add(x);
                }
                else
                {
                    isCrossing = false;
                }
            }

            return result.ToArray();
        }
    }
}
